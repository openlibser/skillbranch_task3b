const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const getPetsJson = require('./getPetsJson');
const allowCrossDomain = require('./skillBranchCORS');
const generateRoutes = require('./generateRoutes');

const app = express();

app.use(allowCrossDomain);
app.use(morgan('tiny'));

// const getPetsByType = (type) => {
//
// }

getPetsJson().then((petsInfo) => {
  const filePath = `./jsons_bak/pets_${Math.random().toString().slice(3, 8)}.json`;
  fs.writeFileSync(filePath, JSON.stringify(petsInfo, '', 2));

  app.get('/', (req, res) => {
    res.json(petsInfo);
  });

  app.get('/users', (req, res, next) => {
    if (!req.query.havePet) return next();

    const petType = req.query.havePet;
    const usersIds = petsInfo.pets
      .filter(pet => pet.type === petType)
      .map(pet => pet.userId);

    const usersWithPet = petsInfo.users.filter(user => usersIds.includes(user.id));
    return res.json(usersWithPet);
  });

  app.get('/users/populate', (req, res) => {
    let filtredUsers = petsInfo.users;

    if (req.query.havePet) {
      const petType = req.query.havePet;
      const usersIds = petsInfo.pets
        .filter(pet => pet.type === petType)
        .map(pet => pet.userId);

      filtredUsers = petsInfo.users
        .filter(user => usersIds.includes(user.id));
    }

    const UsersWithPets = filtredUsers.map((usr) => {
      const pets = petsInfo.pets.filter(pet => pet.userId === usr.id);
      return Object.assign({}, usr, { pets });
    });

    res.json(UsersWithPets);
  });

  app.get('/users/:usernameOrId/populate', (req, res) => {
    const isId = !isNaN(+req.params.usernameOrId);

    let filtredUsers = petsInfo.users;
    if (isId) {
      const userId = +req.params.usernameOrId;
      filtredUsers = filtredUsers.filter(usr => usr.id === userId);
    } else {
      const userName = req.params.usernameOrId;
      filtredUsers = filtredUsers.filter(usr => usr.username === userName);
    }

    if (!filtredUsers.length) throw new Error('404');

    const userWithPets = filtredUsers.map((usr) => {
      const pets = petsInfo.pets.filter(pet => pet.userId === usr.id);
      return Object.assign({}, usr, { pets });
    })[0];

    res.json(userWithPets);
  });

  app.get('/users/:username', (req, res, next) => {
    const isNumber = !isNaN(+req.params.username);
    if (isNumber) return next();

    const user = petsInfo.users
      .filter(usr => usr.username === req.params.username)[0];

    if (!user) throw new Error('404');
    return res.json(user);
  });

  app.get('/users/:username/pets', (req, res, next) => {
    const isNumber = !isNaN(+req.params.username);
    if (isNumber) return next();

    const user = petsInfo.users
      .filter(usr => usr.username === req.params.username)[0];
    if (!user) throw new Error('404');

    const userPets = petsInfo.pets.filter(pet => pet.userId === user.id);
    if (!userPets.length) throw new Error('404');

    return res.json(userPets);
  });

  app.get('/pets', (req, res, next) => {
    const isFilterParams = req.query.type || req.query.age_gt || req.query.age_lt;
    if (!isFilterParams) {
      return next();
    }

    let filtredPets = petsInfo.pets;
    if (req.query.type) {
      filtredPets = filtredPets.filter(pet => pet.type === req.query.type);
    }
    if (req.query.age_gt) {
      filtredPets = filtredPets.filter(pet => pet.age > +req.query.age_gt);
    }
    if (req.query.age_lt) {
      filtredPets = filtredPets.filter(pet => pet.age < +req.query.age_lt);
    }

    return res.json(filtredPets);
  });

  app.get('/pets/populate', (req, res) => {
    const petsWithUsers = petsInfo.pets.map((pet) => {
      const user = petsInfo.users.filter(usr => usr.id === pet.userId)[0];
      return Object.assign({}, pet, { user });
    });

    let filtredPets = petsWithUsers;
    if (req.query.type) {
      filtredPets = filtredPets.filter(pet => pet.type === req.query.type);
    }
    if (req.query.age_gt) {
      filtredPets = filtredPets.filter(pet => pet.age > req.query.age_gt);
    }
    if (req.query.age_lt) {
      filtredPets = filtredPets.filter(pet => pet.age < req.query.age_lt);
    }

    res.json(filtredPets);
  });


  app.get('/pets/:id/populate', (req, res, next) => {
    if (isNaN(+req.params.id)) return next();

    const pet = petsInfo.pets.filter(pet => pet.id === +req.params.id)[0];
    if (!pet) throw new Error('404');

    const petWithUser = Object.assign({}, pet, {
      user: petsInfo.users.filter(usr => usr.id === pet.userId)[0],
    });

    res.json(petWithUser);
  });

  const generatedRoutes = generateRoutes(petsInfo);
  console.log(generatedRoutes);
  const ERROR_MESSAGE_404 = 'Not Found';

  for (const route of Object.keys(generatedRoutes)) {
    app.get(route, generatedRoutes[route]);
  }

  app.get('*', (req, res) => {
    res.status(404).send(ERROR_MESSAGE_404);
  });

  // routes error handling
  app.use((err, req, res, next) => {
    if (err && err.message === '404') {
      res.status(404).send(ERROR_MESSAGE_404);
    } else if (err) {
      res.status(500).send('Server error.');
      console.error(err);
    }
    next();
  });

  app.listen(3000, () => console.log('Server runing on 3000 port!'));
})
.catch(console.error.bind(console));
