const fetch = require('isomorphic-fetch');

const url = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

async function getPetsJson() {
  const response = await fetch(url);
  const pets = await response.json();

  return pets;
}

module.exports = getPetsJson;
